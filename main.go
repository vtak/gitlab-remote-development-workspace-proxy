package main

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	proxyPort = 8000
)

var (
	routingSuffix string
	headerKey     = "X-GitLab-Workspace-Proxy"
	headerValue   = "gitlab-remote-development-workspace-proxy"
)

// Proxy to define the proxy functionality
type Proxy struct{}

func main() {
	var found bool
	routingSuffix, found = os.LookupEnv("ROUTING_SUFFIX")
	if found == false {
		fmt.Printf("ROUTING_SUFFIX is not defined\n")
	}
	// Listen on the predefined proxy port.
	http.ListenAndServe(fmt.Sprintf(":%d", proxyPort), &Proxy{})
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// Forward the HTTP request to the destination service.
	res, duration, err := p.forwardRequest(req)

	// Notify the client if there was an error while forwarding the request.
	if err != nil {
		fmt.Printf("could not forward request: %v", err)
		// Set a special header to notify that the proxy actually serviced the request.
		w.Header().Set(headerKey, headerValue)
		http.Error(w, err.Error(), http.StatusBadGateway)
		return
	}

	// If the request was forwarded successfully, write the response back to
	// the client.
	err = p.writeResponse(w, res)
	// Notify the client if there was an error while writing the response.
	if err != nil {
		fmt.Printf("could not write the response: %v", err)
		// Set a special header to notify that the proxy actually serviced the request.
		w.Header().Set(headerKey, headerValue)
		http.Error(w, err.Error(), http.StatusBadGateway)
		return
	}

	// Print request and response statistics.
	p.printStats(req, res, duration)
}

func (p *Proxy) extractPort(req *http.Request) (string, error) {
	if routingSuffix != "" && strings.HasSuffix(req.Host, routingSuffix) {
		// Request is coming from ingress and the format would be
		// "workspaceId-endpointName-endpointPort.routingSuffix".
		x := strings.Split(req.Host, fmt.Sprintf(".%s", routingSuffix))
		y := strings.Split(x[0], "-")
		return y[len(y)-1], nil
	}
	// Request is either coming from service URL or pod IP.
	_, port, err := net.SplitHostPort(req.Host)
	return port, err
}

func (p *Proxy) forwardRequest(req *http.Request) (*http.Response, time.Duration, error) {
	// Print the original URL.
	fmt.Printf("Original URL: http://%s%s\n", req.Host, req.RequestURI)

	// Prepare the destination endpoint to forward the request to.
	port, err := p.extractPort(req)
	if err != nil {
		return nil, 0, err
	}
	proxyUrl := fmt.Sprintf("http://127.0.0.1:%s%s", port, req.RequestURI)
	fmt.Printf("Proxy URL: %s\n", proxyUrl)

	// Create an HTTP client and a proxy request based on the original request.
	httpClient := http.Client{}
	proxyReq, err := http.NewRequest(req.Method, proxyUrl, req.Body)
	if err != nil {
		return nil, 0, err
	}

	// Capture the duration while making a request to the destination service.
	start := time.Now()
	res, err := httpClient.Do(proxyReq)
	duration := time.Since(start)

	// Return the response, the request duration, and the error.
	return res, duration, err
}

func (p *Proxy) writeResponse(w http.ResponseWriter, res *http.Response) error {
	// Defer finishing the request.
	defer res.Body.Close()

	// Copy all the header values from the response.
	for name, values := range res.Header {
		w.Header()[name] = values
	}

	// Set a special header to notify that the proxy actually serviced the request.
	w.Header().Set(headerKey, headerValue)

	// Set the status code returned by the destination service.
	w.WriteHeader(res.StatusCode)

	// Copy the contents from the response body.
	_, err := io.Copy(w, res.Body)
	if err != nil {
		return err
	}

	return nil
}

func (p *Proxy) printStats(req *http.Request, res *http.Response, duration time.Duration) {
	fmt.Printf("Request Duration: %v\n", duration)
	fmt.Printf("Request Size: %d\n", req.ContentLength)
	fmt.Printf("Response Size: %d\n", res.ContentLength)
	fmt.Printf("Response Status: %d\n\n", res.StatusCode)
}
