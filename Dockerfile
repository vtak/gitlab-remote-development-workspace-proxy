# Use the Go v1.18 image for the base
FROM golang:1.18-alpine

# Create a working directory inside the image
WORKDIR /app

# Copy Go modules and dependencies to image
COPY go.mod ./

# Download Go modules and dependencies
RUN go mod download

# Copy directory files i.e all files ending with .go
COPY *.go ./

# Compile application
RUN go build -o ./gitlab-remote-development-workspace-proxy


# Use the alpine v3.17.0 image for the base
FROM alpine:3.17.0

# Create a working directory inside the image
WORKDIR /app

# Copy build from previous stage
COPY --from=0 /app/gitlab-remote-development-workspace-proxy ./

# Run the proxy on container startup
CMD [ "./gitlab-remote-development-workspace-proxy"]

# Expose the proxy port
EXPOSE 8000
