# gitlab-remote-development-workspace-proxy

This is a proof-of-concept to show how we will intercept traffic inside a workspace, 
perform an action(authentication/authorization/verification) and forward traffic to destined port transparently. 

Each VM or Pod in Kubernetes has its own network namespace. 
Thus, they get access to at least 2 network interfaces i.e. `lo` and `eth0`. 
Interface `lo` stands for loopback and `eth0` for ethernet. 
A thing to note here is that these are virtual and not real interfaces. 

`iptables` are used to map one port to another. 
The idea here is map the application port (3000 and/or 5000) on the `eth0` interface to the proxy port (8000). 
This will ensure that the traffic from outside the VM/Pod is routed to the proxy 
whenever it tries to access the application via port 3000 and/or 5000. 
We let the `lo` interface to route VM/Pod-internal traffic directly to the destination application. 

For VM, cloud providers already allow a way to specify custom script to be run on
initialization. In Kubernetes, you run init containers prior to running regular containers in a Pod. 
We use an init container to set up the Pod networking in order to set up the necessary iptables rules. 
This init container needs to be run as `root` user but all other containers are run as `non-root` user.

For this proof of concept, we'll focus on Kubernetes.

### Docker build and push

```shell
make docker-build-and-push
```

### Test

It is assumed you have a running Kubernetes cluster with `ingress-nginx` installed 
and exposed as type LoadBalancer on host IP `192.168.0.121` for the purposes of testing.

```shell
# Deploy the application.
kubectl apply -f ./deploy.yaml

# Export variables to easily test.
export NAMESPACE=default
export APPLICATION_NAME=gitlab-remote-development-proxy-test
export PORT_1=3000
export PORT_2=5000
export POD_IP=$(kubectl get pod $APPLICATION_NAME -o jsonpath='{.status.podIP}')
export SERVICE_IP=$(kubectl get svc $APPLICATION_NAME-service -o jsonpath='{.spec.clusterIP}')
export SERVICE_URL=${APPLICATION_NAME}-service.${NAMESPACE}.svc.cluster.local
export INGRESS_HOST_1=$(kubectl get ing $APPLICATION_NAME-ingress -o jsonpath='{.spec.rules[0].host}')
export INGRESS_HOST_2=$(kubectl get ing $APPLICATION_NAME-ingress -o jsonpath='{.spec.rules[1].host}')
```

Tail the pod logs to verify each request being received by the workspace-proxy

```shell
kubectl logs -f $APPLICATION_NAME -c workspace-proxy
```

In each of the following test instance, check the response header 
`X-GitLab-Workspace-Proxy: gitlab-remote-development-workspace-proxy` .
Please note, the time taken is for the "busybox" pod to be scheduled to a node in kubernetes. 
It is not the delay due to the workspace-proxy. 
You can verify this by keeping a watch on the pods being scheduled `kubectl get po -n ${NAMESPACE} --watch`

#### Accessing through Pod IP from within the cluster

```shell
# Get requests
kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i ${POD_IP}:${PORT_1}/get?query=param"
  
kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i ${POD_IP}:${PORT_2}/get?query=param"

# Post requests
kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i -X POST -d body=parameters ${POD_IP}:${PORT_1}/post"
  
kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i -X POST -d body=parameters ${POD_IP}:${PORT_2}/post"

# Verify behaviour for various HTTP response codes which the service application could respond with
kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i ${POD_IP}:${PORT_1}/status/429"

kubectl run -i --rm --restart=Never busybox --image=odise/busybox-curl -n ${NAMESPACE} \
  -- sh -c "curl -i ${POD_IP}:${PORT_2}/status/429"
```

#### Accessing through Service IP from within the cluster

Replace `${POD_IP}` with `${SERVICE_IP}` and run the same commands as above.

#### Accessing through Service URL from within the cluster

Replace `${POD_IP}` with `${SERVICE_URL}` and run the same commands as above.

#### Accessing through Ingress URL from within the cluster

Replace `${POD_IP}:${PORT_1}` with `${INGRESS_HOST_1}` and
`${POD_IP}:${PORT_2}` with `${INGRESS_HOST_2}` and 
run the same commands as above.

#### Accessing through Ingress IP from outside the cluster

Replace `${POD_IP}:${PORT_1}` with `${INGRESS_HOST_1}` and
`${POD_IP}:${PORT_2}` with `${INGRESS_HOST_2}` and
run the curl command directly.

### Reference
- https://venilnoronha.io/hand-crafting-a-sidecar-proxy-and-demystifying-istio
