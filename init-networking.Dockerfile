# Use the alpine v3.17.0 image for the base
FROM alpine:3.17.0

# Create a working directory inside the image
WORKDIR /app

# Install the iptables command
RUN apk update && \
    apk add --no-cache iptables \
    rm -rf /var/cache/apk/*

# Copy the initialization script into the container
COPY init.sh .

# Mark the initialization script as executable
RUN chmod +x ./init.sh

# Start the initialization script on container startup
CMD ["./init.sh"]
