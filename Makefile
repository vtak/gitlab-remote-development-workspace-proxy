REPOSITORY_NAME := registry.gitlab.com/vtak/gitlab-remote-development-workspace-proxy
PROXY_IMAGE_NAME := $(REPOSITORY_NAME)/proxy
INIT_NETWORKING_IMAGE_NAME := $(REPOSITORY_NAME)/init-networking

docker-login-gitlab:
	docker login registry.gitlab.com

docker-build-proxy-init-networking:
	docker build -t $(INIT_NETWORKING_IMAGE_NAME) -f init-networking.Dockerfile .

docker-build-proxy:
	docker build -t $(PROXY_IMAGE_NAME) -f Dockerfile .

docker-build: docker-build-proxy-init-networking docker-build-proxy

docker-push-proxy-init-networking: docker-login-gitlab
	docker push $(INIT_NETWORKING_IMAGE_NAME)

docker-push-proxy: docker-login-gitlab
	docker push $(PROXY_IMAGE_NAME)

docker-push: docker-push-proxy-init-networking docker-push-proxy

docker-build-and-push: docker-build docker-push
