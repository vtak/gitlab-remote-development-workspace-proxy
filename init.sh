#!/bin/sh

# Forward TCP traffic on port 3000 to port 8000 on the eth0 interface
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 3000 -j REDIRECT --to-port 8000

# Forward TCP traffic on port 5000 to port 8000 on the eth0 interface
iptables -t nat -A PREROUTING -p tcp -i eth0 --dport 5000 -j REDIRECT --to-port 8000

# List all iptables rules
iptables -t nat --list
